# -*- coding: utf-8 -*-
{
    "name": "Cubes Technical Interview",
    "author": "Mohamed Yosef",
    "category": "Tests",
    "version": "1.0",
    "depends": [
        "base",
        "project",
        "hr",
    ],
    "data": [
        "security/ir.model.access.csv",
        "views/hr_employee_inherit_views.xml",
        "views/project_project_inherit_views.xml",
        "reports/templates.xml",
        "reports/ir_actions_report.xml",
    ],
    "installable": True,
    "license": "LGPL-3",
}
