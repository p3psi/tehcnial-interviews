from odoo import http
from odoo.http import request


class CubesTechnicalInterview(http.Controller):
    @http.route("/hr_employee/get_all_employees_active_projects", type="http", auth="user", methods=["GET"])
    def get_employees_active_projects(self):
        employees = request.env["hr.employee"].sudo().search([])
        data = [
            {
                "employee_name": emp.name,
                "active_projects": [collaborator.project_id.name for collaborator in emp.collaborator_project_ids if collaborator.status == "active"],
            }
            for emp in employees
            if any(collaborator.status == "active" for collaborator in emp.collaborator_project_ids)
        ]

        if not data:
            return request.make_json_response({"message": "No employees with active projects found.", "code": 200})

        response_data = {"code": 200, "data": data}
        return request.make_json_response(response_data)
