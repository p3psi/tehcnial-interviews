from odoo import fields
from odoo import models


class Project(models.Model):
    _inherit = "project.project"

    odoo_version = fields.Integer(string="Odoo Version")
    odoo_type = fields.Selection(
        selection=[
            ("community", "Community"),
            ("enterprise", "Enterprise"),
        ],
        string="Odoo Type",
        default="enterprise",
    )
    github_repo = fields.Char(string="Github Repository")
    github_url = fields.Char(string="Github URL")
    hosting = fields.Selection(
        selection=[
            ("on_perm", "On Premises"),
            ("cloud_hosting", "Cloud Hosting"),
            ("odoo_sh", "Odoo SH"),
            ("odoo_online", "Odoo Online"),
        ]
    )
    hosting_description = fields.Html("Hosting Description")
    collaborator_ids = fields.One2many(comodel_name="cubes.collaborators", string="Collaborators", inverse_name="project_id")
