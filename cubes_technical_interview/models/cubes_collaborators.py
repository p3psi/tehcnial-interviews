from odoo import fields
from odoo import models


class Collaborators(models.Model):
    _name = "cubes.collaborators"
    _description = "Github Project Collaborators"

    employee_id = fields.Many2one("hr.employee", string="Employee", required=True)
    project_id = fields.Many2one(comodel_name="project.project", string="Projects", required=True)
    status = fields.Selection(
        selection=[
            ("inactive", "Inactive"),
            ("active", "Active"),
        ],
        string="Status",
        default="inactive",
        required=True,
    )

    def set_active(self):
        for record in self:
            record.status = "active"

    def set_inactive(self):
        for record in self:
            record.status = "inactive"
