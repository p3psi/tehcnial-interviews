from odoo import _
from odoo import fields
from odoo import models
from odoo.exceptions import UserError


class HREmployee(models.Model):
    _inherit = "hr.employee"

    github_account = fields.Char(string="Github Account")
    collaborator_project_ids = fields.One2many("cubes.collaborators", compute="_compute_collaborator_projects", string="Projects", inverse_name="employee_id")

    def _compute_collaborator_projects(self):
        for record in self:
            collaborator_projects = self.env["cubes.collaborators"].search([("employee_id", "=", record.id)])
            record.collaborator_project_ids = [(6, 0, collaborator_projects.ids)]

    def toggle_active(self):
        active_project_exists = self.env["cubes.collaborators"].search_count(
            [
                ("employee_id", "in", self.ids),
                ("status", "=", "active"),
            ]
        )

        if active_project_exists:
            raise UserError(_("You can't archive employees who are active collaborators on projects."))

        return super(HREmployee, self).toggle_active()
